package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Collection<Integer> syncCollection = Collections.synchronizedCollection(new ArrayList<>());
        Random random = new Random();
        Runnable listOperations = () -> {
            for (int i = 0; i < 5000; i++)
                syncCollection.add(random.nextInt(11));
        };

        Thread thread1 = new Thread(listOperations);
        Thread thread2 = new Thread(listOperations);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

        System.out.println(syncCollection.stream().filter(x -> x == 5).count());
    }
}
