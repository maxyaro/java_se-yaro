package com.company;

class Enemy {
    protected int health;
    protected int dmg;
    protected String type;

    public Enemy()
    {
        health = 200;
        dmg = 20;
    }

    public void Attack()
    {
        doDamage(dmg);
    }

    protected  void doDamage(int dmg)
    {
    
    }

}
class CoolEnemy extends  Enemy {
    public CoolEnemy()
    {
        super();
    }

    @Override
    protected void doDamage(int dmg) {
        super.doDamage(dmg *20);
    }
}

class RubbishEnemy extends  Enemy {
    public RubbishEnemy()
    {
        super();
    }
    @Override
    protected void doDamage(int dmg) {
        super.doDamage(dmg /20);
    }
}
